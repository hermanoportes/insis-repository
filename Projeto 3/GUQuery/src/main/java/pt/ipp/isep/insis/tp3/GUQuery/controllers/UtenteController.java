package pt.ipp.isep.insis.tp3.GUQuery.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ipp.isep.insis.tp3.GUQuery.entities.Utente;
import pt.ipp.isep.insis.tp3.GUQuery.services.UtenteService;
import pt.ipp.isep.insis.tp3.GUQuery.services.SenderService;

import java.net.URI;

@RestController
@RequestMapping("/api/utente")
public class UtenteController {

    @Autowired
    private UtenteService utenteService;

    @Autowired
    private SenderService senderService;

    @GetMapping
    public ResponseEntity<Iterable<Utente>> findAll() {

        Iterable<Utente> listaUtentes = utenteService.findAll();
        return ResponseEntity.ok().body(listaUtentes);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Utente> findById(@PathVariable Integer id) {

        Utente utente = utenteService.findById(id);
        return ResponseEntity.ok().body(utente);
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<Utente> insert(@RequestParam String utente, @RequestParam String obra,
                                  @RequestParam String autor, @RequestParam String polo, @RequestParam String dataInicio,
                                  @RequestParam String dataFim) {

        Utente utente1 = utenteService.insert(utente, obra, autor, polo, dataInicio, dataFim);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(utente1.getId())
                .toUri();
        senderService.sendUtenteAdicionada(utente1);
        return ResponseEntity.created(uri).body(utente1);
    }

    @PostMapping("/")
    public @ResponseBody
    ResponseEntity<Utente> insert(@RequestBody Utente utente) {

        utente = utenteService.insert(utente);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(utente.getId())
                .toUri();
        return ResponseEntity.created(uri).body(utente);
    }

    @PutMapping("/up/{id}")
    public ResponseEntity<Utente> updateById(@PathVariable Integer id, @RequestParam String utente,
                                             @RequestParam String obra, @RequestParam String autor, @RequestParam String polo,
                                             @RequestParam String dataInicio, @RequestParam String dataFim) {

        Utente utente1 = utenteService.updateById(id, utente, obra, autor, polo, dataInicio, dataFim);
        return ResponseEntity.ok().body(utente1);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Utente> updateById(@PathVariable Integer id, @RequestBody Utente utente) {

        utente = utenteService.updateById(id, utente);
        return ResponseEntity.ok().body(utente);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

        utenteService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
