package pt.ipp.isep.insis.project.util;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.bpmn.core.types.datatypes.json.api.JsonNodeObject;

import com.fasterxml.jackson.databind.JsonNode;

public class DefinirPrecoDeReferencia implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(DefinirPrecoDeReferencia.class);

	public void execute(DelegateExecution execution) throws Exception {
		
		log.info("escolhendo melhor preco...");
		
		JsonNodeObject json = (JsonNodeObject)execution.getVariable("listPrecos");
		
		List<JsonNode> listPrecos= new ArrayList<JsonNode>();
		json.findValues("value", listPrecos);
		
		List<Double> precosEncontrados = new ArrayList<Double>();
		Double soma = 0D;
		
		for(JsonNode preco : listPrecos) {
			log.info("preco: $" + preco);
			precosEncontrados.add(preco.doubleValue());
			soma += preco.doubleValue();
		}
		
		String precoReferencia = Double.toString((Math.round(soma / precosEncontrados.size() * 100.0)/100.0));
		
		log.info("preco de referencia: $" + precoReferencia);
		execution.setVariable("precoDeReferencia", precoReferencia);
	}

}
