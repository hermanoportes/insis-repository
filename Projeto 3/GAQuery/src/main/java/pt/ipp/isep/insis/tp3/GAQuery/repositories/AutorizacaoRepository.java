package pt.ipp.isep.insis.tp3.GAQuery.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.insis.tp3.GAQuery.entities.Autorizacao;

public interface AutorizacaoRepository extends JpaRepository<Autorizacao, Integer> {

}
