package pt.ipp.isep.insis.project.util;

import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FindPeritosRegistradosJson implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(FindPeritosRegistradosJson.class);

	public void execute(DelegateExecution execution) throws Exception {
		log.info("buscando peritos registrados...");

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

		List<User> users = processEngine.getIdentityService().createUserQuery().orderByUserId().asc().list();

		String peritosRegistrados;
		peritosRegistrados = "[{\"_id\":\"\",\"text\":\"\"}";
		for (int i = 0; i < users.size(); i++ ) {
			if(users.get(i).getId().contains("perito")) {
				log.info("perito: " + users.get(i).getId());
				if(i < users.size()) peritosRegistrados += ",";
				peritosRegistrados += ("{\"_id\":\"" + users.get(i).getId() + "\",\"text\":\"" + users.get(i).getId() + "\"}");
			}
		}
		peritosRegistrados += "]";
		execution.setVariable("peritosRegistrados", peritosRegistrados);
	}

}