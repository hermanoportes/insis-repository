package pt.ipp.isep.insis.tp3.GECommand.services;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GECommand.entities.Emprestimo;

import static pt.ipp.isep.insis.tp3.GECommand.GECommandApplication.log;

@Service
public class ReceiverService {

    @Autowired
    private EmprestimoService emprestimoService;

    @RabbitListener(queues = "ge.command")
    public void consumeGeCommand(Emprestimo emprestimo) {

        log.info("Menssagem recebida: " + emprestimo.toString());
        emprestimo = emprestimoService.insert(emprestimo);
    }

}
