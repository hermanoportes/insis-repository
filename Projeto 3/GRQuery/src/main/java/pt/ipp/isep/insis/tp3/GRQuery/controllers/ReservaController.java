package pt.ipp.isep.insis.tp3.GRQuery.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ipp.isep.insis.tp3.GRQuery.entities.Reserva;
import pt.ipp.isep.insis.tp3.GRQuery.services.ReservaService;
import pt.ipp.isep.insis.tp3.GRQuery.services.SenderService;

import java.net.URI;

@RestController
@RequestMapping("/api/reserva/v1")
public class ReservaController {

    @Autowired
    private ReservaService reservaService;

    @Autowired
    private SenderService senderService;

    @GetMapping
    public ResponseEntity<Iterable<Reserva>> findAll() {

        Iterable<Reserva> listaReservas = reservaService.findAll();
        return ResponseEntity.ok().body(listaReservas);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Reserva> findById(@PathVariable Integer id) {

        Reserva reserva = reservaService.findById(id);
        return ResponseEntity.ok().body(reserva);
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<Reserva> insert(@RequestParam String utente, @RequestParam String obra,
                                   @RequestParam String autor, @RequestParam String polo, @RequestParam String dataInicio,
                                   @RequestParam String dataFim) {

        Reserva reserva = reservaService.insert(utente, obra, autor, polo, dataInicio, dataFim);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(reserva.getId())
                .toUri();
        senderService.sendReservaAdicionada(reserva);
        return ResponseEntity.created(uri).body(reserva);
    }

    @PostMapping("/")
    public @ResponseBody
    ResponseEntity<Reserva> insert(@RequestBody Reserva reserva) {

        reserva = reservaService.insert(reserva);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(reserva.getId())
                .toUri();
        return ResponseEntity.created(uri).body(reserva);
    }

    @PutMapping("/up/{id}")
    public ResponseEntity<Reserva> updateById(@PathVariable Integer id, @RequestParam String utente,
                                              @RequestParam String obra, @RequestParam String autor, @RequestParam String polo,
                                              @RequestParam String dataInicio, @RequestParam String dataFim) {

        Reserva reserva = reservaService.updateById(id, utente, obra, autor, polo, dataInicio, dataFim);
        return ResponseEntity.ok().body(reserva);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Reserva> updateById(@PathVariable Integer id, @RequestBody Reserva reserva) {

        reserva = reservaService.updateById(id, reserva);
        return ResponseEntity.ok().body(reserva);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

        reservaService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
