package pt.ipp.isep.insis.project.util;

import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PreparaPeritosProponente implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(PreparaPeritosProponente.class);

	public void execute(DelegateExecution execution) throws Exception {
		log.info("buscando peritos escolhidos pelo proponente...");

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

		List<User> users = processEngine.getIdentityService().createUserQuery().orderByUserId().asc().list();
		String escolhidos = "";
		for (int i = 1; i <= users.size(); i++ ) {
			String escolha = "peritoProponente" + i;
			String escolhido = execution.getVariable(escolha, String.class);
			if(escolhido != null) escolhidos += ("," + escolhido);
		}

		log.info(escolhidos.toString());		
		execution.setVariable("peritosEscolhidos", escolhidos);
	}

}