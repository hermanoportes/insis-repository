package pt.ipp.isep.insis.tp3.GUCommand.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.insis.tp3.GUCommand.entities.Utente;

public interface UtenteRepository extends JpaRepository<Utente, Integer> {

}
