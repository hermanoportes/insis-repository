package pt.ipp.isep.insis.tp3.GAQuery.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ipp.isep.insis.tp3.GAQuery.entities.Autorizacao;
import pt.ipp.isep.insis.tp3.GAQuery.services.AutorizacaoService;
import pt.ipp.isep.insis.tp3.GAQuery.services.SenderService;

import java.net.URI;

@RestController
@RequestMapping("/api/autorizacao")
public class AutorizacaoController {

    @Autowired
    private AutorizacaoService autorizacaoService;

    @Autowired
    private SenderService senderService;

    @GetMapping
    public ResponseEntity<Iterable<Autorizacao>> findAll() {

        Iterable<Autorizacao> listaAutorizacoes = autorizacaoService.findAll();
        return ResponseEntity.ok().body(listaAutorizacoes);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Autorizacao> findById(@PathVariable Integer id) {

        Autorizacao autorizacao = autorizacaoService.findById(id);
        return ResponseEntity.ok().body(autorizacao);
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<Autorizacao> insert(@RequestParam String utente, @RequestParam String obra,
                                       @RequestParam String autor, @RequestParam String polo, @RequestParam String dataInicio,
                                       @RequestParam String dataFim) {

        Autorizacao autorizacao = autorizacaoService.insert(utente, obra, autor, polo, dataInicio, dataFim);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(autorizacao.getId())
                .toUri();
        senderService.sendAutorizacaoAdicionada(autorizacao);
        return ResponseEntity.created(uri).body(autorizacao);
    }

    @PostMapping("/")
    public @ResponseBody
    ResponseEntity<Autorizacao> insert(@RequestBody Autorizacao autorizacao) {

        autorizacao = autorizacaoService.insert(autorizacao);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(autorizacao.getId())
                .toUri();
        return ResponseEntity.created(uri).body(autorizacao);
    }

    @PutMapping("/up/{id}")
    public ResponseEntity<Autorizacao> updateById(@PathVariable Integer id, @RequestParam String utente,
                                                  @RequestParam String obra, @RequestParam String autor, @RequestParam String polo,
                                                  @RequestParam String dataInicio, @RequestParam String dataFim) {

        Autorizacao autorizacao = autorizacaoService.updateById(id, utente, obra, autor, polo, dataInicio, dataFim);
        return ResponseEntity.ok().body(autorizacao);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Autorizacao> updateById(@PathVariable Integer id, @RequestBody Autorizacao autorizacao) {

        autorizacao = autorizacaoService.updateById(id, autorizacao);
        return ResponseEntity.ok().body(autorizacao);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

        autorizacaoService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
