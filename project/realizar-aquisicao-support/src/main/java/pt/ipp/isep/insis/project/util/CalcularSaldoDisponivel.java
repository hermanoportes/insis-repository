package pt.ipp.isep.insis.project.util;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.wso2.carbon.bpmn.core.types.datatypes.json.api.JsonNodeObject;

public class CalcularSaldoDisponivel implements JavaDelegate {

	private static Log log = LogFactory.getLog(CalcularSaldoDisponivel.class);

	public void execute(DelegateExecution execution) throws Exception {
		
		log.info("calculando saldo disponivel para aquisicoes...");
		
		JsonNodeObject json = (JsonNodeObject)execution.getVariable("saldoContaAquisicao");
				
		double saldoContaAquisicao = json.findValue("saldo").asDouble();
		
		String saldoDisponivel = Double.toString((Math.round(saldoContaAquisicao * 100.0)/100.0));
		
		log.info("saldo Conta Aquisicao: " + saldoContaAquisicao);
		
		execution.setVariable("saldoDisponivel", saldoDisponivel);
	}

}
