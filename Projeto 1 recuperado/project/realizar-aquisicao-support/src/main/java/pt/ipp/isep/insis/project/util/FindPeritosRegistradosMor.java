package pt.ipp.isep.insis.project.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FindPeritosRegistradosMor implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(FindPeritosRegistradosMor.class);

	public void execute(DelegateExecution execution) throws Exception {
		log.info("buscando peritos nao escolhidos...");

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

		List<User> users = processEngine.getIdentityService().createUserQuery().orderByUserId().asc().list();
		List<String> escolhidos = new ArrayList<String>();

		String escolhidosProponente = execution.getVariable("peritosEscolhidos", String.class);
        String div[] = escolhidosProponente.split(",");
        escolhidos = Arrays.asList(div);		
		
		String peritosNaoEscolhidos;
		peritosNaoEscolhidos = "[{\"_id\":\"\",\"text\":\"\"}";
		for (int i = 0; i < users.size(); i++ ) {
			String user = users.get(i).getId();
			if(user.contains("perito")) {
				if(escolhidos.contains(user)) log.info("perito ja escolhido: " + user);
				else {
					log.info("perito nao escolhido: " + user);
					if(i < users.size()) peritosNaoEscolhidos += ",";
					peritosNaoEscolhidos += ("{\"_id\":\"" + user + "\",\"text\":\"" + user + "\"}");
				}
			}
		} 
		peritosNaoEscolhidos += "]";
		execution.setVariable("peritosNaoEscolhidos", peritosNaoEscolhidos);
	}

}