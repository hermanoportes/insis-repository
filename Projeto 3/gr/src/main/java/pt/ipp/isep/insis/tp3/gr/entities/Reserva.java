package pt.ipp.isep.insis.tp3.gr.entities;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "tb_reserva")
public class Reserva {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotBlank
	private String utente;

	@NotBlank
	private String obra;

	private String autor;

	private String polo;

	@NotBlank
	private Instant dataInicio;

	@NotBlank
	private Instant dataFim;


	public Reserva() {
		super();
	}

	/**
	 * @param utente
	 * @param obra
	 * @param autor
	 * @param polo
	 * @param dataInicio
	 * @param dataFim
	 */
	public Reserva(String utente, String obra, String autor, String polo, Instant dataInicio, Instant dataFim) {
		this.utente = utente;
		this.obra = obra;
		this.autor = autor;
		this.polo = polo;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the utente
	 */
	public String getUtente() {
		return utente;
	}

	/**
	 * @param utente the utente to set
	 */
	public void setUtente(String utente) {
		this.utente = utente;
	}

	/**
	 * @return the obra
	 */
	public String getObra() {
		return obra;
	}

	/**
	 * @param obra the obra to set
	 */
	public void setObra(String obra) {
		this.obra = obra;
	}

	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * @param autor the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * @return the polo
	 */
	public String getPolo() {
		return polo;
	}

	/**
	 * @param polo the polo to set
	 */
	public void setPolo(String polo) {
		this.polo = polo;
	}

	/**
	 * @return the dataInicio
	 */
	public Instant getDataInicio() {
		return dataInicio;
	}

	/**
	 * @param dataInicio the dataInicio to set
	 */
	public void setDataInicio(Instant dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * @return the dataFim
	 */
	public Instant getDataFim() {
		return dataFim;
	}

	/**
	 * @param dataFim the dataFim to set
	 */
	public void setDataFim(Instant dataFim) {
		this.dataFim = dataFim;
	}

}
