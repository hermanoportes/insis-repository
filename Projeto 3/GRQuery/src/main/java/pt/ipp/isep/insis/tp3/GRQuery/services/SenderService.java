package pt.ipp.isep.insis.tp3.GRQuery.services;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import pt.ipp.isep.insis.tp3.GRQuery.entities.Reserva;

import static pt.ipp.isep.insis.tp3.GRQuery.GrQueryApplication.log;

@Service
public class SenderService {

    @Autowired
    private final RabbitTemplate rabbitTemplate;

    public SenderService(RabbitTemplate rabbitTemplate) {

        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendReservaAdicionada(@RequestBody Reserva reserva) {
        try {
            rabbitTemplate.convertAndSend("gr.topic", "v1.reserva.adicionada", reserva);
            log.info("Mensagem enviada: " + reserva);
        } catch (AmqpException e) {
            e.printStackTrace();
        }
    }
}
