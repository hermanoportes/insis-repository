package pt.ipp.isep.insis.tp3.gr.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.ipp.isep.insis.tp3.gr.entities.Reserva;

public interface ReservaRepository extends JpaRepository<Reserva, Integer> {

}
