package pt.ipp.isep.insis.project.util;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FindPeritosRegistrados implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(FindPeritosRegistrados.class);

	public void execute(DelegateExecution execution) throws Exception {
		log.info("buscando peritos registrados...");

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

		List<User> users = processEngine.getIdentityService().createUserQuery().orderByUserId().asc().list();

		List<String> peritosRegistrados = new ArrayList<>();

		for (User user : users) {
			if(user.getId().contains("perito")) {
				log.info("perito: " + user.getId());
				peritosRegistrados.add(user.getId());
			}
		}

		execution.setVariable("peritosRegistrados", peritosRegistrados);
	}

}