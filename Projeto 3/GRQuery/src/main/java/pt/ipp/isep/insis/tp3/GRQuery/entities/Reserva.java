package pt.ipp.isep.insis.tp3.GRQuery.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "tb_reserva")
public class Reserva implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    private String utente;

    @NotBlank
    private String obra;

    private String autor;

    private String polo;

    @NotBlank
    private String dataInicio;

    @NotBlank
    private String dataFim;

    public Reserva(@JsonProperty("utente") String utente, @JsonProperty("obra") String obra, @JsonProperty("autor") String autor, @JsonProperty("polo") String polo,
                   @JsonProperty("dataInicio") String dataInicio, @JsonProperty("dataFim") String dataFim) {
        this.utente = utente;
        this.obra = obra;
        this.autor = autor;
        this.polo = polo;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
    }

    public Reserva(@JsonProperty("id") int id, @JsonProperty("utente") String utente, @JsonProperty("obra") String obra, @JsonProperty("autor") String autor, @JsonProperty("polo") String polo,
                   @JsonProperty("dataInicio") String dataInicio, @JsonProperty("dataFim") String dataFim) {
        this.utente = utente;
        this.obra = obra;
        this.autor = autor;
        this.polo = polo;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    public String getObra() {
        return obra;
    }

    public void setObra(String obra) {
        this.obra = obra;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getPolo() {
        return polo;
    }

    public void setPolo(String polo) {
        this.polo = polo;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    @Override
    public String toString() {
        return "Reserva{" +
                "id=" + id +
                ", utente='" + utente + '\'' +
                ", obra='" + obra + '\'' +
                ", autor='" + autor + '\'' +
                ", polo='" + polo + '\'' +
                ", dataInicio=" + dataInicio +
                ", dataFim=" + dataFim +
                '}';
    }
}
