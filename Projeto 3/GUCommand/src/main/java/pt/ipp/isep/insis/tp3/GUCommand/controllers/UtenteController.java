package pt.ipp.isep.insis.tp3.GUCommand.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ipp.isep.insis.tp3.GUCommand.entities.Utente;
import pt.ipp.isep.insis.tp3.GUCommand.services.SenderService;
import pt.ipp.isep.insis.tp3.GUCommand.services.UtenteService;

import java.net.URI;

@RestController
@RequestMapping("/api/utente")
public class UtenteController {

    @Autowired
    private UtenteService utenteService;

    @Autowired
    private SenderService senderService;

    @GetMapping
    public ResponseEntity<Iterable<Utente>> findAll() {

        Iterable<Utente> listaUtentes = utenteService.findAll();
        return ResponseEntity.ok().body(listaUtentes);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Utente> findById(@PathVariable Integer id) {

        Utente utente = utenteService.findById(id);
        return ResponseEntity.ok().body(utente);
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<Utente> insert(@RequestParam String nome, @RequestParam String estado) {

        Utente utente = utenteService.insert(nome, estado);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(utente.getId())
                .toUri();
        senderService.sendUtenteAdicionada(utente);
        return ResponseEntity.created(uri).body(utente);
    }

    @PostMapping("/")
    public @ResponseBody
    ResponseEntity<Utente> insert(@RequestBody Utente utente) {

        utente = utenteService.insert(utente);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(utente.getId())
                .toUri();
        return ResponseEntity.created(uri).body(utente);
    }

    @PutMapping("/up/{id}")
    public ResponseEntity<Utente> updateEstadoById(@PathVariable Integer id,
                                                   @RequestParam String estado) {

        Utente utente = utenteService.updateEstadoById(id, estado);
        return ResponseEntity.ok().body(utente);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Utente> updateById(@PathVariable Integer id, @RequestBody Utente utente) {

        utente = utenteService.updateUtenteById(id, utente);
        return ResponseEntity.ok().body(utente);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

        utenteService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
