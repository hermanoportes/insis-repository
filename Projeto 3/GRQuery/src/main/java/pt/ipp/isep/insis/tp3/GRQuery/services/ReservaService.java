package pt.ipp.isep.insis.tp3.GRQuery.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GRQuery.entities.Reserva;
import pt.ipp.isep.insis.tp3.GRQuery.repositories.ReservaRepository;
import pt.ipp.isep.insis.tp3.GRQuery.services.exceptions.ResourceNotFoundException;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class ReservaService {

    @Autowired
    private ReservaRepository reservaRepository;

    public Iterable<Reserva> findAll() {

        return reservaRepository.findAll();
    }

    public Reserva findById(Integer id) {

        Optional<Reserva> reserva = reservaRepository.findById(id);
        return reserva.orElseThrow(() -> new ResourceNotFoundException(id));
    }

    public Reserva insert(String utente, String obra, String autor, String polo, String dataInicio, String dataFim) {

        Reserva reserva = new Reserva(utente, obra, autor, polo, dataInicio, dataFim);

        return reservaRepository.save(reserva);
    }

    public Reserva insert(Reserva reserva) {

        return reservaRepository.save(reserva);
    }

    public Reserva updateById(Integer id, String utente, String obra, String autor, String polo, String dataInicio,
                              String dataFim) {

        try {
            Reserva entity = reservaRepository.getOne(id);
            entity.setObra(obra);
            entity.setAutor(autor);
            entity.setPolo(polo);
            entity.setDataInicio(dataInicio);
            entity.setDataFim(dataFim);

            return reservaRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public Reserva updateById(Integer id, Reserva reserva) {

        try {
            Reserva entity = reservaRepository.getOne(id);
            entity.setObra(reserva.getObra());
            entity.setAutor(reserva.getAutor());
            entity.setPolo(reserva.getPolo());
            entity.setDataInicio(reserva.getDataInicio());
            entity.setDataFim(reserva.getDataFim());

            return reservaRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public void deleteById(Integer id) {

        try {
            reservaRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(id);
        }
    }

}
