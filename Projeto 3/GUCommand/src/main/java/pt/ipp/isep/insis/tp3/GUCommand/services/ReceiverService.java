package pt.ipp.isep.insis.tp3.GUCommand.services;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GUCommand.entities.Utente;

import static pt.ipp.isep.insis.tp3.GUCommand.GuCommandApplication.log;

@Service
public class ReceiverService {

    @Autowired
    private UtenteService utenteService;

    @RabbitListener(queues = "gu.command")
    public void consumeGrCommand(Utente utente) {

        log.info("Menssagem recebida: " + utente.toString());
        utente = utenteService.insert(utente);
    }

}
