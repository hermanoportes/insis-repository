package pt.ipp.isep.insis.tp3.GAQuery.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GAQuery.entities.Autorizacao;
import pt.ipp.isep.insis.tp3.GAQuery.repositories.AutorizacaoRepository;
import pt.ipp.isep.insis.tp3.GAQuery.services.exceptions.ResourceNotFoundException;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class AutorizacaoService {

    @Autowired
    private AutorizacaoRepository autorizacaoRepository;

    public Iterable<Autorizacao> findAll() {

        return autorizacaoRepository.findAll();
    }

    public Autorizacao findById(Integer id) {

        Optional<Autorizacao> autorizacao = autorizacaoRepository.findById(id);
        return autorizacao.orElseThrow(() -> new ResourceNotFoundException(id));
    }

    public Autorizacao insert(String utente, String obra, String autor, String polo, String dataInicio, String dataFim) {

        Autorizacao autorizacao = new Autorizacao(utente, obra, autor, polo, dataInicio, dataFim);

        return autorizacaoRepository.save(autorizacao);
    }

    public Autorizacao insert(Autorizacao autorizacao) {

        return autorizacaoRepository.save(autorizacao);
    }

    public Autorizacao updateById(Integer id, String utente, String obra, String autor, String polo, String dataInicio,
                                  String dataFim) {

        try {
            Autorizacao entity = autorizacaoRepository.getOne(id);
            entity.setObra(obra);
            entity.setAutor(autor);
            entity.setPolo(polo);
            entity.setDataInicio(dataInicio);
            entity.setDataFim(dataFim);

            return autorizacaoRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public Autorizacao updateById(Integer id, Autorizacao autorizacao) {

        try {
            Autorizacao entity = autorizacaoRepository.getOne(id);
            entity.setObra(autorizacao.getObra());
            entity.setAutor(autorizacao.getAutor());
            entity.setPolo(autorizacao.getPolo());
            entity.setDataInicio(autorizacao.getDataInicio());
            entity.setDataFim(autorizacao.getDataFim());

            return autorizacaoRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public void deleteById(Integer id) {

        try {
            autorizacaoRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(id);
        }
    }

}
