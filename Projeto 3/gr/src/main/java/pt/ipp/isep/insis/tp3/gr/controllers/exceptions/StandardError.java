package pt.ipp.isep.insis.tp3.gr.controllers.exceptions;

import java.time.Instant;

public class StandardError {

	private Instant timetamp;
	private Integer status;
	private String error;
	private String message;
	private String path;

	public StandardError() {
	}

	/**
	 * @param timetamp
	 * @param status
	 * @param error
	 * @param message
	 * @param path
	 */
	public StandardError(Instant timetamp, Integer status, String error, String message, String path) {
		super();
		this.timetamp = timetamp;
		this.status = status;
		this.error = error;
		this.message = message;
		this.path = path;
	}

	/**
	 * @return the timetamp
	 */
	public Instant getTimetamp() {
		return timetamp;
	}

	/**
	 * @param timetamp the timetamp to set
	 */
	public void setTimetamp(Instant timetamp) {
		this.timetamp = timetamp;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}


	
}
