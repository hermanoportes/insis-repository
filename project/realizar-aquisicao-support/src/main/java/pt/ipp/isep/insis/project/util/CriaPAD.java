package pt.ipp.isep.insis.project.util;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CriaPAD implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(CriaPAD.class);

	public void execute(DelegateExecution execution) throws Exception {
		log.info("cria PAD...");

		String PAD = execution.getVariable("PAD", String.class);
		
		PAD = "Solicito o aquisição do livro " + execution.getVariable("tituloPublicacao", String.class);
		PAD += " do autor " + execution.getVariable("autorPublicacao", String.class);
		PAD += " publicado em " + execution.getVariable("anoPublicacao", Long.class);
		PAD += " para o polo " + execution.getVariable("poloSugerido", String.class);
		PAD += ". O melhor preço de cotação foi de " + execution.getVariable("valorEstimado", String.class) + " euros";
		
		log.info(PAD.toString());
		execution.setVariable("PAD", PAD);
	}

}