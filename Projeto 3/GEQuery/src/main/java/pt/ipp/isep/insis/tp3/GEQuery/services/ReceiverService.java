package pt.ipp.isep.insis.tp3.GEQuery.services;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GEQuery.entities.Emprestimo;

import static pt.ipp.isep.insis.tp3.GEQuery.GEQueryApplication.log;

@Service
public class ReceiverService {

    @Autowired
    private EmprestimoService emprestimoService;

    @RabbitListener(queues = "ge.query")
    public void consumeGeQuery(Emprestimo emprestimo) {

        log.info("Menssagem recebida: " + emprestimo.toString());
        emprestimo = emprestimoService.insert(emprestimo);
    }

}
