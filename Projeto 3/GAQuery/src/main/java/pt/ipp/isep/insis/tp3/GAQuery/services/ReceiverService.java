package pt.ipp.isep.insis.tp3.GAQuery.services;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GAQuery.entities.Autorizacao;

import static pt.ipp.isep.insis.tp3.GAQuery.GAQueryApplication.log;

@Service
public class ReceiverService {

    @Autowired
    private AutorizacaoService autorizacaoService;

    @RabbitListener(queues = "ga.query")
    public void consumeGrQuery(Autorizacao autorizacao) {

        log.info("Menssagem recebida: " + autorizacao.toString());
        autorizacao = autorizacaoService.insert(autorizacao);
    }

}
