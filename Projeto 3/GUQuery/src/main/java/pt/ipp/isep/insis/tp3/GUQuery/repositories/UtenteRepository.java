package pt.ipp.isep.insis.tp3.GUQuery.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.insis.tp3.GUQuery.entities.Utente;

public interface UtenteRepository extends JpaRepository<Utente, Integer> {

}
