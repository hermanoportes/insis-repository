package pt.ipp.isep.insis.tp3.GRCommand.services;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GRCommand.entities.Reserva;

import static pt.ipp.isep.insis.tp3.GRCommand.GrCommandApplication.log;

@Service
public class ReceiverService {

    @Autowired
    private ReservaService reservaService;

    @RabbitListener(queues = "gr.command")
    public void consumeGrQuery(Reserva reserva) {

        log.info("Menssagem recebida: " + reserva.toString());
        reserva = reservaService.insert(reserva);
    }

}
