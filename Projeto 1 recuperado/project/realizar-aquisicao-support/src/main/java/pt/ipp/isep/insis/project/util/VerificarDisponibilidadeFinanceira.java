package pt.ipp.isep.insis.project.util;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class VerificarDisponibilidadeFinanceira implements JavaDelegate {

	private static Log log = LogFactory.getLog(VerificarDisponibilidadeFinanceira.class);

	public void execute(DelegateExecution execution) throws Exception {
		
		log.info("verificando disponibilidade financeira...");
		
		String titulo = execution.getVariable("tituloPublicacao", String.class);


		String precoDeReferencia = execution.getVariable("precoDeReferencia", String.class);
		
		double valorTotal = Double.valueOf(precoDeReferencia);
		
		Double saldoDisponivel = Double.valueOf(execution.getVariable("saldoDisponivel", String.class));
		
		boolean disponibilidadeFinanceira = false;
		String msg = "Reprovado";
		
		if(valorTotal <= saldoDisponivel) {
			disponibilidadeFinanceira = true;
			msg = "Aprovado";
		}
	
		log.info("Titulo: " + titulo);
		log.info("Valor Total: " + valorTotal);
		log.info("Saldo Disponivel: " + saldoDisponivel);	
		log.info("Disponibilidade financeira: " + msg);		
		
		execution.setVariable("disponibilidadeFinanceira", disponibilidadeFinanceira);
		execution.setVariable("msg", msg);
	}

}
