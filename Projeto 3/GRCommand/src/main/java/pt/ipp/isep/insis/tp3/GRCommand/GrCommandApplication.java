package pt.ipp.isep.insis.tp3.GRCommand;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GrCommandApplication implements CommandLineRunner {

    public static final Logger log = LoggerFactory.getLogger(GrCommandApplication.class);

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    public static void main(String[] args) {

        SpringApplication.run(GrCommandApplication.class, args);
    }

    @Bean
    public TopicExchange grQueryExchange() {
        return new TopicExchange("gr.topic");
    }

    @Bean
    public Queue grQuery() {
        return new Queue("gr.command");
    }

    @Bean
    public Binding binding1() {
        return BindingBuilder.bind(grQuery()).to(grQueryExchange()).with("v1.reserva.adicionada");
    }

    @Override
    public void run(String... strings) throws Exception {
        log.info("Início do serviço de reserva Command v1");
    }

}
