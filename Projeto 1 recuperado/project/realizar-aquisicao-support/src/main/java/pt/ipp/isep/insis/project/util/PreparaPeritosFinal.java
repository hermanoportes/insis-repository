package pt.ipp.isep.insis.project.util;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PreparaPeritosFinal implements JavaDelegate {
	
	private static Log log = LogFactory.getLog(PreparaPeritosFinal.class);

	public void execute(DelegateExecution execution) throws Exception {
		log.info("buscando peritos escolhidos para o parecer...");

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		List<User> users = processEngine.getIdentityService().createUserQuery().orderByUserId().asc().list();
		
		String escolhidos = execution.getVariable("peritosEscolhidos", String.class);
		
		for (int i = 1; i <= users.size(); i++ ) {
			String escolha = "peritoMor" + i;
			String escolhido = execution.getVariable(escolha, String.class);
			if(escolhido != null) escolhidos += ("," + escolhido);
		}
		
		List<String> escolhidosFinal = new ArrayList<>();
		
        String div[] = escolhidos.split(",");
        
		for (int i = 0; i < div.length; i++) {
			if(div[i].contains("perito") && !escolhidosFinal.contains(div[i])) {
				escolhidosFinal.add(div[i]);
			}
		}
		log.info(escolhidosFinal.toString());
		execution.setVariable("escolhidosFinal", escolhidosFinal);
	}

}