package pt.ipp.isep.insis.tp3.GECommand.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GECommand.entities.Emprestimo;
import pt.ipp.isep.insis.tp3.GECommand.repositories.EmprestimoRepository;
import pt.ipp.isep.insis.tp3.GECommand.services.exceptions.ResourceNotFoundException;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class EmprestimoService {

    @Autowired
    private EmprestimoRepository emprestimoRepository;

    public Iterable<Emprestimo> findAll() {

        return emprestimoRepository.findAll();
    }

    public Emprestimo findById(Integer id) {

        Optional<Emprestimo> emprestimo = emprestimoRepository.findById(id);
        return emprestimo.orElseThrow(() -> new ResourceNotFoundException(id));
    }

    public Emprestimo insert(String utente, String obra, String autor, String polo, String dataInicio, String dataFim) {

        Emprestimo emprestimo = new Emprestimo(utente, obra, autor, polo, dataInicio, dataFim);

        return emprestimoRepository.save(emprestimo);
    }

    public Emprestimo insert(Emprestimo emprestimo) {

        return emprestimoRepository.save(emprestimo);
    }

    public Emprestimo updateById(Integer id, String utente, String obra, String autor, String polo, String dataInicio,
                                 String dataFim) {

        try {
            Emprestimo entity = emprestimoRepository.getOne(id);
            entity.setObra(obra);
            entity.setAutor(autor);
            entity.setPolo(polo);
            entity.setDataInicio(dataInicio);
            entity.setDataFim(dataFim);

            return emprestimoRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public Emprestimo updateById(Integer id, Emprestimo emprestimo) {

        try {
            Emprestimo entity = emprestimoRepository.getOne(id);
            entity.setObra(emprestimo.getObra());
            entity.setAutor(emprestimo.getAutor());
            entity.setPolo(emprestimo.getPolo());
            entity.setDataInicio(emprestimo.getDataInicio());
            entity.setDataFim(emprestimo.getDataFim());

            return emprestimoRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public void deleteById(Integer id) {

        try {
            emprestimoRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(id);
        }
    }

}
