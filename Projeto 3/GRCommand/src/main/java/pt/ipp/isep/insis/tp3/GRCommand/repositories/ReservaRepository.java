package pt.ipp.isep.insis.tp3.GRCommand.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.insis.tp3.GRCommand.entities.Reserva;

public interface ReservaRepository extends JpaRepository<Reserva, Integer> {

}
