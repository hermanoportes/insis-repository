package pt.ipp.isep.insis.tp3.GUCommand.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "tb_utente")
public class Utente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    private String nome;

    @NotBlank
    private String estado;

    public Utente(@JsonProperty("nome") String nome, @JsonProperty("estado") String estado) {
        this.nome = nome;
        this.estado = estado;
    }

    public Utente(@JsonProperty("id") int id, @JsonProperty("nome") String nome, @JsonProperty("estado") String estado) {
        this.nome = nome;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Utente{" +
                "id=" + id +
                ", utente='" + nome + '\'' +
                ", obra='" + estado + '\'' +
                '}';
    }
}
