package pt.ipp.isep.insis.tp3.GECommand.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.insis.tp3.GECommand.entities.Emprestimo;

public interface EmprestimoRepository extends JpaRepository<Emprestimo, Integer> {

}
