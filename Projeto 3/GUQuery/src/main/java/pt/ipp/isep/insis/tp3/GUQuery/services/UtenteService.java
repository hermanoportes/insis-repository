package pt.ipp.isep.insis.tp3.GUQuery.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GUQuery.entities.Utente;
import pt.ipp.isep.insis.tp3.GUQuery.repositories.UtenteRepository;
import pt.ipp.isep.insis.tp3.GUQuery.services.exceptions.ResourceNotFoundException;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UtenteService {

    @Autowired
    private UtenteRepository utenteRepository;

    public Iterable<Utente> findAll() {

        return utenteRepository.findAll();
    }

    public Utente findById(Integer id) {

        Optional<Utente> utente = utenteRepository.findById(id);
        return utente.orElseThrow(() -> new ResourceNotFoundException(id));
    }

    public Utente insert(String utente, String obra, String autor, String polo, String dataInicio, String dataFim) {

        Utente utente1 = new Utente(utente, obra, autor, polo, dataInicio, dataFim);

        return utenteRepository.save(utente1);
    }

    public Utente insert(Utente utente) {

        return utenteRepository.save(utente);
    }

    public Utente updateById(Integer id, String utente, String obra, String autor, String polo, String dataInicio,
                             String dataFim) {

        try {
            Utente entity = utenteRepository.getOne(id);
            entity.setObra(obra);
            entity.setAutor(autor);
            entity.setPolo(polo);
            entity.setDataInicio(dataInicio);
            entity.setDataFim(dataFim);

            return utenteRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public Utente updateById(Integer id, Utente utente) {

        try {
            Utente entity = utenteRepository.getOne(id);
            entity.setObra(utente.getObra());
            entity.setAutor(utente.getAutor());
            entity.setPolo(utente.getPolo());
            entity.setDataInicio(utente.getDataInicio());
            entity.setDataFim(utente.getDataFim());

            return utenteRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public void deleteById(Integer id) {

        try {
            utenteRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(id);
        }
    }

}
