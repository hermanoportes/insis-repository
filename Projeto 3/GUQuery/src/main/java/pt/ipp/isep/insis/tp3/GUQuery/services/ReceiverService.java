package pt.ipp.isep.insis.tp3.GUQuery.services;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GUQuery.entities.Utente;

import static pt.ipp.isep.insis.tp3.GUQuery.GuQueryApplication.log;

@Service
public class ReceiverService {

    @Autowired
    private UtenteService utenteService;

    @RabbitListener(queues = "gu.query")
    public void consumeGrQuery(Utente utente) {

        log.info("Menssagem recebida: " + utente.toString());
        utente = utenteService.insert(utente);
    }

}
