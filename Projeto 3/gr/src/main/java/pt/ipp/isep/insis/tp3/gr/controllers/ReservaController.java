package pt.ipp.isep.insis.tp3.gr.controllers;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import pt.ipp.isep.insis.tp3.gr.entities.Reserva;
import pt.ipp.isep.insis.tp3.gr.services.ReservaService;

@RestController
@RequestMapping("/api/reserva")
public class ReservaController {

	@Autowired
	private ReservaService reservaService;

	@GetMapping
	public ResponseEntity<Iterable<Reserva>> findAll() {

		Iterable<Reserva> listaReservas = reservaService.findAll();
		return ResponseEntity.ok().body(listaReservas);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Reserva> findById(@PathVariable Integer id) {

		Reserva reserva = reservaService.findById(id);
		return ResponseEntity.ok().body(reserva);
	}

	@PostMapping
	public @ResponseBody ResponseEntity<Reserva> insert(@RequestParam String utente, @RequestParam String obra,
			@RequestParam String autor, @RequestParam String polo, @RequestParam String dataInicio,
			@RequestParam String dataFim) {

		Reserva reserva = reservaService.insert(utente, obra, autor, polo, dataInicio, dataFim);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(reserva.getId())
				.toUri();
		return ResponseEntity.created(uri).body(reserva);
	}

	@PostMapping("/")
	public @ResponseBody ResponseEntity<Reserva> insert(@RequestBody Reserva reserva) {

		reserva = reservaService.insert(reserva);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(reserva.getId())
				.toUri();
		return ResponseEntity.created(uri).body(reserva);
	}

	@PutMapping("/up/{id}")
	public ResponseEntity<Reserva> updateById(@PathVariable Integer id, @RequestParam String utente,
			@RequestParam String obra, @RequestParam String autor, @RequestParam String polo,
			@RequestParam String dataInicio, @RequestParam String dataFim) {

		Reserva reserva = reservaService.updateById(id, utente, obra, autor, polo, dataInicio, dataFim);
		return ResponseEntity.ok().body(reserva);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Reserva> updateById(@PathVariable Integer id, @RequestBody Reserva reserva) {

		reserva = reservaService.updateById(id, reserva);
		return ResponseEntity.ok().body(reserva);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

		reservaService.deleteById(id);
		return ResponseEntity.noContent().build();
	}

}
