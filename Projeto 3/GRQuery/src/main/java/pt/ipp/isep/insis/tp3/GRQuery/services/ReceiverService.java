package pt.ipp.isep.insis.tp3.GRQuery.services;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GRQuery.entities.Reserva;

import static pt.ipp.isep.insis.tp3.GRQuery.GrQueryApplication.log;

@Service
public class ReceiverService {

    @Autowired
    private ReservaService reservaService;

    @RabbitListener(queues = "gr.query")
    public void consumeGrQuery(Reserva reserva) {

        log.info("Menssagem recebida: " + reserva.toString());
        reserva = reservaService.insert(reserva);
    }

}
