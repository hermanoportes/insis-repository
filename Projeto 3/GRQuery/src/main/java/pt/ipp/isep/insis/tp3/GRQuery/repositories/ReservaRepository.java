package pt.ipp.isep.insis.tp3.GRQuery.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.insis.tp3.GRQuery.entities.Reserva;

public interface ReservaRepository extends JpaRepository<Reserva, Integer> {

}
