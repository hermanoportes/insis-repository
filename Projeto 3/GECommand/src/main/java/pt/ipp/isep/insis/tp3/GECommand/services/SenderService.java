package pt.ipp.isep.insis.tp3.GECommand.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.bind.annotation.RequestBody;
import pt.ipp.isep.insis.tp3.GECommand.entities.Emprestimo;

@Service
public class SenderService {

    private static final Logger log = LoggerFactory.getLogger(SenderService.class);

    @Autowired
    private final   RabbitTemplate rabbitTemplate;

    public SenderService(RabbitTemplate rabbitTemplate) {

        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendEmprestimoAdicionado(@RequestBody Emprestimo emprestimo) {
        try {
            rabbitTemplate.convertAndSend("ge.topic", "emprestimo.adicionado", emprestimo);
            log.info("Mensagem enviada: " + emprestimo);
        } catch (AmqpException e) {
            e.printStackTrace();
        }
    }
}
