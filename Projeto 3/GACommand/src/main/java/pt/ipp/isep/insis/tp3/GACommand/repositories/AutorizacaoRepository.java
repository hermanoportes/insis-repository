package pt.ipp.isep.insis.tp3.GACommand.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.insis.tp3.GACommand.entities.Autorizacao;

public interface AutorizacaoRepository extends JpaRepository<Autorizacao, Integer> {

}
