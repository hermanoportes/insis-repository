package pt.ipp.isep.insis.tp3.GECommand.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ipp.isep.insis.tp3.GECommand.entities.Emprestimo;
import pt.ipp.isep.insis.tp3.GECommand.services.EmprestimoService;
import pt.ipp.isep.insis.tp3.GECommand.services.SenderService;

import java.net.URI;

@RestController
@RequestMapping("/api/emprestimo")
public class EmprestimoController {

    @Autowired
    private EmprestimoService emprestimoService;

    @Autowired
    private SenderService senderService;

    @GetMapping
    public ResponseEntity<Iterable<Emprestimo>> findAll() {

        Iterable<Emprestimo> listaEmprestimos = emprestimoService.findAll();
        return ResponseEntity.ok().body(listaEmprestimos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Emprestimo> findById(@PathVariable Integer id) {

        Emprestimo emprestimo = emprestimoService.findById(id);
        return ResponseEntity.ok().body(emprestimo);
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<Emprestimo> insert(@RequestParam String utente, @RequestParam String obra,
                                      @RequestParam String autor, @RequestParam String polo, @RequestParam String dataInicio,
                                      @RequestParam String dataFim) {

        Emprestimo emprestimo = emprestimoService.insert(utente, obra, autor, polo, dataInicio, dataFim);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(emprestimo.getId())
                .toUri();
        senderService.sendEmprestimoAdicionado(emprestimo);
        return ResponseEntity.created(uri).body(emprestimo);
    }

    @PostMapping("/")
    public @ResponseBody
    ResponseEntity<Emprestimo> insert(@RequestBody Emprestimo emprestimo) {

        emprestimo = emprestimoService.insert(emprestimo);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(emprestimo.getId())
                .toUri();
        return ResponseEntity.created(uri).body(emprestimo);
    }

    @PutMapping("/up/{id}")
    public ResponseEntity<Emprestimo> updateById(@PathVariable Integer id, @RequestParam String utente,
                                                 @RequestParam String obra, @RequestParam String autor, @RequestParam String polo,
                                                 @RequestParam String dataInicio, @RequestParam String dataFim) {

        Emprestimo emprestimo = emprestimoService.updateById(id, utente, obra, autor, polo, dataInicio, dataFim);
        return ResponseEntity.ok().body(emprestimo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Emprestimo> updateById(@PathVariable Integer id, @RequestBody Emprestimo emprestimo) {

        emprestimo = emprestimoService.updateById(id, emprestimo);
        return ResponseEntity.ok().body(emprestimo);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

        emprestimoService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
