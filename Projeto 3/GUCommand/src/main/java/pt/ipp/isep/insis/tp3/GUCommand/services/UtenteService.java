package pt.ipp.isep.insis.tp3.GUCommand.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import pt.ipp.isep.insis.tp3.GUCommand.entities.Utente;
import pt.ipp.isep.insis.tp3.GUCommand.repositories.UtenteRepository;
import pt.ipp.isep.insis.tp3.GUCommand.services.exceptions.ResourceNotFoundException;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UtenteService {

    @Autowired
    private UtenteRepository utenteRepository;

    public Iterable<Utente> findAll() {

        return utenteRepository.findAll();
    }

    public Utente findById(Integer id) {

        Optional<Utente> utente = utenteRepository.findById(id);
        return utente.orElseThrow(() -> new ResourceNotFoundException(id));
    }

    public Utente insert(String nome, String estado) {

        Utente utente = new Utente(nome, estado);

        return utenteRepository.save(utente);
    }

    public Utente insert(Utente utente) {

        return utenteRepository.save(utente);
    }

//    public Utente updateNomeById(Integer id, String nome) {
//
//        try {
//            Utente entity = utenteRepository.getOne(id);
//            entity.setNome(nome);
//
//            return utenteRepository.save(entity);
//
//        } catch (EntityNotFoundException e) {
//            throw new ResourceNotFoundException(id);
//        }
//    }

    public Utente updateEstadoById(Integer id, String estado) {

        try {
            Utente entity = utenteRepository.getOne(id);
            entity.setEstado(estado);

            return utenteRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public Utente updateUtenteById(Integer id, Utente utente) {

        try {
            Utente entity = utenteRepository.getOne(id);
            entity.setNome(utente.getNome());
            entity.setEstado(utente.getEstado());

            return utenteRepository.save(entity);

        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public void deleteById(Integer id) {

        try {
            utenteRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(id);
        }
    }

}
