package pt.ipp.isep.insis.project.form.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ParecerListener implements TaskListener {
	
	private static final long serialVersionUID = 1L;

	private static final String PERITOS_ESCOLHIDOS = "peritosEscolhidos";
	private static final String PARECER_PERITO = "parecerPerito";
	private static final String RESULTADO_PARECER = "resultadoParecer";

	private static Log log = LogFactory.getLog(ParecerListener.class);

	public Expression variableNameExpression;

	private static final String APROVADO_PERITOS = "aprovadoPeritos";
	private long parecerAprovado;
	private long parecerRejeitado;

	@SuppressWarnings("unchecked")
	public void notify(DelegateTask delegateTask) {
		log.info("colhendo pareceres...");

		List<String> resultado = new ArrayList<>();

		if (delegateTask.hasVariable(RESULTADO_PARECER)) {
			resultado = (List<String>) delegateTask.getVariable(RESULTADO_PARECER);
		}

		String parecer = delegateTask.getVariable(PARECER_PERITO, String.class);
		String perito = delegateTask.getVariable(PERITOS_ESCOLHIDOS, String.class);

		log.info(perito + ": " + parecer);

		if(parecer.equalsIgnoreCase("true")) {
			resultado.add(perito + ": Aprova");
			parecerAprovado ++;
		}
		else if(parecer.equalsIgnoreCase("false")) {
			resultado.add(perito + ": Rejeita");
			parecerRejeitado ++;
		}

		delegateTask.setVariable(RESULTADO_PARECER, resultado);
		
		if(parecerAprovado > parecerRejeitado) delegateTask.setVariable(APROVADO_PERITOS, "true");
		else delegateTask.setVariable(APROVADO_PERITOS, "false");
	}

}
