package pt.ipp.isep.insis.tp3.GEQuery.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.insis.tp3.GEQuery.entities.Emprestimo;

public interface EmprestimoRepository extends JpaRepository<Emprestimo, Integer> {

}
