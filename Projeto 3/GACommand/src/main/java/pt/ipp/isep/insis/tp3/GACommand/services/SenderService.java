package pt.ipp.isep.insis.tp3.GACommand.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.bind.annotation.RequestBody;
import pt.ipp.isep.insis.tp3.GACommand.entities.Autorizacao;

@Service
public class SenderService {

    private static final Logger log = LoggerFactory.getLogger(SenderService.class);

    @Autowired
    private final   RabbitTemplate rabbitTemplate;

    public SenderService(RabbitTemplate rabbitTemplate) {

        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendAutorizacaoAdicionada(@RequestBody Autorizacao autorizacao) {
        try {
            rabbitTemplate.convertAndSend("ga.topic", "autorizacao.adicionada", autorizacao);
            log.info("Mensagem enviada: " + autorizacao);
        } catch (AmqpException e) {
            e.printStackTrace();
        }
    }
}
